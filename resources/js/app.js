
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vuex from 'vuex';
Vue.use(Vuex);

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

import VueRouter from 'vue-router'
Vue.use(VueRouter);

Vue.use(require('vue-moment'));

import {router} from './route/router';
import {store} from './store';

import Toastr from 'vue-toastr';
require('vue-toastr/src/vue-toastr.scss');
Vue.component('vue-toastr', Toastr);

Vue.component('validation-errors', require('./components/partials/ValidationErrors.vue').default);
Vue.component('pagination', require('./components/partials/Pagination.vue').default);
Vue.component('site-header', require('./components/partials/SiteHeader.vue').default);


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store,
    template: `<div><router-view :key="+$store.state.key + '_' + $route.fullPath.replace(/\\?tab=\\d/, '')"></router-view><vue-toastr ref="toastr"></vue-toastr></div>`,

    data() {

        return {
            key: 0
        }
    },

    created(){

        axios.interceptors.response.use(null, (error) => {

            if (+error.response.status === 401 && this.$store.state.auth.user) {

                this.$store.commit('auth/unsetToken');
                this.$store.commit('auth/unsetUser');

                return this.$router.push({name: 'login'});
            }
            return Promise.reject(error);
        });
    },


});
