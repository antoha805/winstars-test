export const formMixin = {
    data () {
        return {
            isStoring: false,
            serverValidationErrors: {},
            formFields: {},
        }
    },

    created() {


    },
    methods: {

        resetFlags(){

            for (let key of Object.keys(this.fields)){

                this.$validator.flag(key, {
                    "untouched": true,
                    "touched": false,
                    "dirty": false,
                    "valid": true,
                    "invalid": false,
                    "validated": false
                });
            }
        },
        beforeStoreHandler(){
            this.serverValidationErrors = {};
            this.isStoring = true;
        },
        submitSuccessHandler(response, message = null){
            this.isStoring = false;

            if ((response.data && response.data.message) || message) {

                this.$root.$refs.toastr.s((response.data && response.data.message) || message);
            }
        },
        submitErrorHandler(error, message = null){

            this.isStoring = false;

            if (error.response && error.response.data && error.response.data.errors) {

                this.serverValidationErrors = error.response.data.errors;
            }

            if ((error.response && error.response.data && error.response.data.message) || message || error.message) {

                this.$root.$refs.toastr.e((error.response && error.response.data && error.response.data.message) || message || error.message);
            }
        },
        getErrors(name, scope = null){

            let errors = [];

            if (this.errors.has(`${scope ? `${scope}.` : ''}${name}`)){

                errors.push(this.errors.first(`${scope ? `${scope}.` : ''}${name}`));
            }

            if (this.serverValidationErrors[name]){

                for (let index in this.serverValidationErrors[name]){
                    errors.push(this.serverValidationErrors[name][index]);
                }
            }

            return errors;
        },
        resetForm() {
            this.$validator.reset();
            this.serverValidationErrors = {};
            this.formFields = {};
        }
    },
};
