// getters
const getters = {

}

const state = {
    token: localStorage.getItem('token'),
    user: null
}

// actions
const actions = {

    async login({ state, commit, rootState }, data) {

        return new Promise((resolve, reject) => {

            axios.post('/api/auth/login', data).then((response) => {

                commit('setToken', response.data.access_token);

                let base64Url = response.data.access_token.split('.')[1];
                let base64 = base64Url.replace('-', '+').replace('_', '/');

                commit('setUser', JSON.parse(atob(base64)));

                resolve(response);

            }).catch((error) => {

                reject(error);
            });
        });
    },

    async logout({ state, commit, rootState }, data) {

        return new Promise((resolve, reject) => {

            axios.post('/api/auth/logout', data).then((response) => {

                commit('unsetUser');
                commit('unsetToken');

                resolve(response);

            }).catch((error) => {

                reject(error);
            });
        });
    },

    async getUser({ state, commit, rootState }){

        return new Promise((resolve, reject) => {

            axios.get(`/api/auth/me`).then((response) => {

                commit('setUser', response.data);
                resolve(response);

            }).catch((error) => {

                commit('unsetUser');
                commit('unsetToken');

                reject(error);
            });
        });
    },

};

// mutations
const mutations = {

    setToken (state, token) {
        state.token = token;
        localStorage.setItem('token', token);
    },

    setUser (state, user) {
        state.user = user;
    },

    unsetToken (state) {
        localStorage.removeItem('token');
        state.token = null;
    },

    unsetUser (state) {
        state.user = null;
    },

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
