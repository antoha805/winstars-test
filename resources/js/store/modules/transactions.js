// getters
const getters = {

}

const state = {
    items: null,
    count: null
}

// actions
const actions = {

    async getItems({ state, commit, rootState }, params) {

        return new Promise((resolve, reject) => {

            axios.get('/api/transactions', {params}).then((response) => {

                commit('setItems', response.data);

                commit('setCount', response.headers.count);

                resolve(response);

            }).catch((error) => {

                reject(error);
            });
        });
    },
};

// mutations
const mutations = {

    setItems (state, items) {
        state.items = items;
    },

    setCount (state, count) {
        state.count = count;
    },

    unsetItems (state) {
        state.items = null;
    },

    unsetCount (state) {
        state.count = null;
    },

};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
