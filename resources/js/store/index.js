import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import transactions from './modules/transactions'

Vue.use(Vuex);

export const store = new Vuex.Store({
        modules: {
            auth,
            transactions
        },

        actions: {

        },
        mutations: {

        },
    })
;
