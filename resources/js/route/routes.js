import Login from '../components/Login.vue';
import Transaction from '../components/Transactions.vue';

export const routes = [
    {
        path: '/',
        name: 'transactions',
        component: Transaction,
        meta: {requiresAuth: true}
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {guestsOnly: true}
    }
];
