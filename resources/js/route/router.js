import {routes} from './routes'
import {store} from '../store';
import VueRouter from 'vue-router'

export const router = new VueRouter({
    routes: routes,
    mode: 'history',
    scrollBehavior (to, from, savedPosition) {

        return { x: 0, y: 0 }
    }
});

router.beforeEach(async (to, from, next) => {

    let token = store.state.auth.token;

    if (token){
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    }

    if (to.meta.requiresAuth) {

        if (!token) {
            next({name: 'login'});
        }
        else if (!store.state.auth.user){

            store.dispatch('auth/getUser').then((response) => {

                next();
            }).catch((error) => {

                next({name: 'login'})
            });
        }
        else {

            next();
        }
    }
    else if (to.meta.guestsOnly) {

        if (!token) {
            next();
        }
        else if (!store.state.user){

            store.dispatch('auth/getUser').then((response) => {

                next({name: 'transactions'});
            }).catch((error) => {

                next()
            });
        }
        else{

            next({name: 'transactions'});
        }
    }

    else if (token && !store.state.auth.user){

        store.dispatch('auth/getUser').then((response) => {

            next();
        });
    }
    else {
        next();
    }
});
