<?php

namespace App\Console\Commands;

use App\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;

class StoreTransactionsSum extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store_transaction_sum';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store the sum of all transactions from previous day.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    public function handle()
    {
        $yesterday = Carbon::yesterday();

        $data = Transaction::select(
            DB::raw("count(id) as transaction_count, sum(amount) as amount_sum")
        )
            ->where('created_at', '>=', $yesterday)
            ->where('created_at', '<', Carbon::today())
            ->first();

        Log::info('transaction daily statistic',
            [
                'date' => $yesterday->toDateString(),
                'transaction_count' => $data->transaction_count,
                'amount_sum' => $data->amount_sum ?? 0
            ]
        );
    }
}
