<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'amount',
    ];

    public function customer(){

        return $this->belongsTo('App\Client');
    }

    public function scopeFilter($builder, $filters)
    {
        return $filters->apply($builder);
    }
}
