<?php

namespace App\Http\Controllers;

use App\Filters\TranslationFilter;
use App\Http\Requests\TransactionRequest;
use App\Http\Resources\TransactionResource;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function show(Request $request, $id)
    {
        return new TransactionResource(Transaction::findOrFail($id));
    }

    public function show2(Request $request, $customerId, $transactionId)
    {
        return new TransactionResource(Transaction::findOrFail($transactionId));
    }

    public function index(Request $request, TranslationFilter $filters)
    {
        $this->validate($request, [
            'customerId' => 'integer|min:1',
            'amount' => 'numeric|min:0.01',
            'date' => 'date_format:d.m.Y',
            'offset' => 'integer|min:0',
            'limit' => 'integer|min:1'
        ]);

        $transaction = Transaction::filter($filters);

        $count = $transaction->count();

        if ($request->input('offset')){
            $transaction = $transaction->offset($request->input('offset'));
        }

        $transaction = $transaction->limit($request->input('limit') ?? 1000);

        return (TransactionResource::collection($transaction->get()))
            ->response()
            ->header('Count', $count);
    }

    public function store(TransactionRequest $request)
    {
        $transaction = Transaction::create([
            'customer_id' => $request->input('customerId'),
            'amount' => $request->input('amount')
        ]);

        return (new TransactionResource($transaction))
            ->response()
            ->setStatusCode(201);
    }

    public function update(TransactionRequest $request, $id)
    {
        $transaction = Transaction::findOrFail($id);

        $transaction->update($request->only('amount'));

        return new TransactionResource($transaction);
    }

    public function destroy(Request $request, $id)
    {
        $transaction = Transaction::findOrFail($id);

        $transaction->delete();

        return [];
    }
}
