<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'transactionId' => $this->id,
            'customerId' => $this->customer_id,
//            'amount' => (float) $this->amount,
            'amount' => $this->amount,
            'date' => Carbon::parse($this->created_at)->format('d.m.Y')
        ];
    }
}
