<?php
namespace App\Filters;

use Carbon\Carbon;

class TranslationFilter extends QueryFilter
{
    public function customerId($value)
    {
        $this->builder->where('customer_id', $value);
    }

    public function amount($value)
    {
        $this->builder->where('amount', $value);
    }

    public function date($value)
    {
        $date = Carbon::createFromFormat('d.m.Y', $value);

        $this->builder->where('created_at', '>=', $date)
            ->where('created_at', '<', $date->copy()->addDay());
    }
}
