<?php
namespace App\Filters;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
abstract class QueryFilter
{
    protected $request;
    protected $builder;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function apply($builder)
    {
        $this->builder = $builder;
        foreach ($this->filters() as $filter => $value) {

            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }

            elseif (method_exists($this, camel_case($filter))) {

                $filterCased = camel_case($filter);

                $this->$filterCased($value);
            }
        }
        return $this->builder;
    }
    public function filters()
    {
        return $this->request->all();
    }
}
