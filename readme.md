For deploy project
- Run composer install
- Create .env file with same context as .env.example file.
- Run php artisan key:generate
- Paste your DB setting in .env file
- Run php artisan migrate:refresh --seed for creating tables with test data.
  Default user: email: admin@admin.com, password: secret
- Run php artisan jwt:secret

- For api authorization use header: Authorization: Bearer eyJhbGciOiJIUzI1NiI...
  where eyJhbGciOiJIUzI1NiI... - your jwt token.
  (https://jwt-auth.readthedocs.io/en/develop/quick-start/#authenticated-requests)
