<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');

});

Route::post('customers', 'CustomerController@store');

Route::resource('transactions', 'TransactionController')->except([
    'create', 'edit'
]);

// I think route 'transactions/{transactionId}' is better for this. But I also leaved 'transaction/{customerId}/{transactionId}'
// for compatibility (router 'transaction/{customerId}/{transactionId}' specified in task document as example)

Route::get('transaction/{customerId}/{transactionId}', 'TransactionController@show2');
