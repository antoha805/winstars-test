<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Customer::class, 200)->create()->each(function ($customer) {

            foreach (range(1, 20) as $i) {

                DB::table('transactions')->insert(
                    array_merge(
                        factory(App\Transaction::class)->make(['customer_id' => $customer->id])->toArray(),
                        ['created_at' => now()->subSecond(rand(0, 60*60*24*30*12*5))]
                )
                );
            }

        });;
    }
}
