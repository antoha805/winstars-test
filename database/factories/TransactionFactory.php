<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Transaction::class, function (Faker $faker) {
    return [
        'customer_id' => function () {
            // Get random genre id
            return App\Customer::inRandomOrder()->first()->id;
        },
        'amount' => $faker->randomFloat(2, 0.01, 100000),
    ];
});
